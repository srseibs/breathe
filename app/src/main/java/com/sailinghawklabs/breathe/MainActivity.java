package com.sailinghawklabs.breathe;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.NumberPicker;
import android.widget.TextView;

import com.github.florent37.viewanimator.AnimationListener;
import com.github.florent37.viewanimator.ViewAnimator;
import com.sailinghawklabs.breathe.util.Prefs;

public class MainActivity extends AppCompatActivity {

    // "4-7-8" breathing technique - Google it
    final int INHALE_DURATION_MS = 4000;
    final int HOLD_DURATION_MS = 7000;
    final int EXHALE_DURATION_MS = 8000;

    //  number Picker
    final int MIN_NUM_BREATHS = 2;
    final int DEFAULT_NUM_BREATHS = 4;
    final int MAX_NUM_BREATHS = 10;
    final float MINS_PER_BREATH = (INHALE_DURATION_MS + HOLD_DURATION_MS + EXHALE_DURATION_MS) / (float)(60 * 1000);

    // animation constants
    final float ALPHA_CHANGE = 0.15f;
    final float SCALE_CHANGE = 0.25f;
    final float ROTATION = 180;

    // my views
    private ImageView mImageView;
    private TextView mNumBreathsTv;
    private TextView mLastTimeTv;
    private TextView mMinsTodayTv;
    private TextView mBreathCounterTv;
    private TextView mBreathePromptTv;
    private NumberPicker mSessionBreathsNp;
    private Button mStartButton;

    // misc
    private Prefs mPrefs;
    private int mBreathCount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mImageView = findViewById(R.id.imageView);
        mNumBreathsTv = findViewById(R.id.breathsTaken);
        mLastTimeTv = findViewById(R.id.lastBreathAt);
        mMinsTodayTv = findViewById(R.id.todaysMins);
        mBreathePromptTv = findViewById(R.id.guideTxt);
        mStartButton = findViewById(R.id.startButton);
        mSessionBreathsNp = findViewById(R.id.numberPicker);
        mBreathCounterTv = findViewById(R.id.breathCounterTv);

        // setup the Number Picker
        mSessionBreathsNp.setMinValue(MIN_NUM_BREATHS);
        mSessionBreathsNp.setMaxValue(MAX_NUM_BREATHS);
        mSessionBreathsNp.setValue(DEFAULT_NUM_BREATHS);
        mSessionBreathsNp.setWrapSelectorWheel(false);

        mPrefs = new Prefs(this);
        updateStats();

        mStartButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mBreathCount = mSessionBreathsNp.getValue();
                mBreathCounterTv.setText(String.valueOf(mBreathCount));
                mSessionBreathsNp.setEnabled(false);
                mStartButton.setVisibility(View.INVISIBLE);
                startAnimation();
            }
        });

        startIntroAnimation();
    }

    private void updateStats() {
        mMinsTodayTv.setText(getString(R.string.msg_mins_today, mPrefs.getMinsToday()));
        mNumBreathsTv.setText(getString(R.string.msg_num_breaths, mPrefs.getTotalBreaths()));
        mLastTimeTv.setText(getString(R.string.msg_last_time, mPrefs.getLastDateString()));
    }

    private void startIntroAnimation() {
        ViewAnimator
                .animate(mBreathePromptTv)
                .scale(0, 1)
                .duration(1500)
                .onStart(new AnimationListener.Start() {
                    @Override
                    public void onStart() {
                        mBreathePromptTv.setText(R.string.prompt_breathe);
                    }
                })
                .start();
    }

    private ViewAnimator buildAnimator() {
        return ViewAnimator.animate(mImageView)// INHALE ------------------------------------
                .onStart(new AnimationListener.Start() {
                    @Override
                    public void onStart() {
                        mBreathePromptTv.setText(R.string.prompt_inhale);

                    }
                })
                .duration(INHALE_DURATION_MS)
                .alpha(ALPHA_CHANGE, 1.0f)
                .rotation(-ROTATION)
                .scale(SCALE_CHANGE, 1.0f).andAnimate(mBreathePromptTv).alpha(0f,1f)
                .andAnimate(mBreathePromptTv)
                .alpha(0f, 1.0f) // inhale
                .decelerate()


                .thenAnimate(mBreathePromptTv) //  HOLD ------------------------------------
                .onStart(new AnimationListener.Start() {
                    @Override
                    public void onStart() {
                        mBreathePromptTv.setText(getString(R.string.prompt_hold));
                    }
                })
                .scale(1.0f, 1 + SCALE_CHANGE, 1.0f)
                .duration(HOLD_DURATION_MS)

                .thenAnimate(mImageView) //  EXHALE  -------------------------------------------
                .onStart(new AnimationListener.Start() {
                    @Override
                    public void onStart() {
                        mBreathePromptTv.setText(R.string.prompt_exhale);
                    }
                })
                .duration(EXHALE_DURATION_MS)
                .alpha(1.0f, ALPHA_CHANGE)
                .rotation(ROTATION)
                .scale(1.0f, SCALE_CHANGE)
                .andAnimate(mBreathePromptTv)
                .alpha(1.0f, 0.0f) // exhale
                .decelerate()
                .onStop(new AnimationListener.Stop() {
                    @Override
                    public void onStop() {
                        mBreathePromptTv.setAlpha(1.0f);
                        mBreathePromptTv.setRotation(0);
                        mImageView.setAlpha(1f);
                        mImageView.setRotation(0f);
                        mImageView.setScaleX(1.0f);
                        mImageView.setScaleY(1.0f);
                        animationIsDone();
                    }
                });
    }

    private void startAnimation() {
        ViewAnimator viewAnimator = buildAnimator();
        viewAnimator.start();
    }

    public void animationIsDone() {
        if (--mBreathCount > 0) {
            mBreathCounterTv.setText(String.valueOf(mBreathCount));
            startAnimation();
        } else {
            float minutes = MINS_PER_BREATH * mSessionBreathsNp.getValue();

            if (mPrefs.isItToday()) {
                mPrefs.setMinsToday(mPrefs.getMinsToday() + minutes);
            } else {
                mPrefs.setMinsToday(minutes);
            }

            mPrefs.setTotalBreaths(mPrefs.getTotalBreaths() + mSessionBreathsNp.getValue());
            mPrefs.setLastDate_ms(System.currentTimeMillis());
            updateStats();

            mStartButton.setVisibility(View.VISIBLE);
            mSessionBreathsNp.setEnabled(true);

            startIntroAnimation();
        }
    }
}
