package com.sailinghawklabs.breathe.util;

import android.app.Activity;
import android.content.SharedPreferences;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class Prefs {
    private final String PREFS_TOTAL_BREATH = "TOTAL_BREATHS";
    private final String PREFS_MIN_TODAY = "MINS_TODAY";
    private final String PREFS_LAST_DATE = "DATE";
    private SharedPreferences mSharedPreferences;

    public Prefs(Activity activity) {
        mSharedPreferences = activity.getPreferences(Activity.MODE_PRIVATE);
    }

    public void setTotalBreaths(int breaths) {
        mSharedPreferences.edit().putInt(PREFS_TOTAL_BREATH, breaths).apply();
    }

    public int getTotalBreaths() {
        return mSharedPreferences.getInt(PREFS_TOTAL_BREATH, 0);
    }

    public void setMinsToday(float minutesToday) {
        mSharedPreferences.edit().putFloat(PREFS_MIN_TODAY, minutesToday).apply();
    }

    public float getMinsToday() {
        return mSharedPreferences.getFloat(PREFS_MIN_TODAY, 0f);
    }

    public void setLastDate_ms(long timedate) {
        mSharedPreferences.edit().putLong(PREFS_LAST_DATE, timedate).apply();
    }

    public String getLastDateString() {
        long dateTime =  getLastDate_ms();
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(dateTime);
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd, h:mm a", Locale.US);
        return dateFormat.format(calendar.getTime());
    }

    public long getLastDate_ms() {
        return mSharedPreferences.getLong(PREFS_LAST_DATE, 0);
    }

    public boolean isItToday() {
        long lastDate_ms = getLastDate_ms();
        long now = System.currentTimeMillis();
        SimpleDateFormat dayNumFormat = new SimpleDateFormat("D", Locale.US); // "D" is day-in-year

        Calendar calendar = Calendar.getInstance();

        calendar.setTimeInMillis(now);
        String now_dayNum  = dayNumFormat.format(calendar.getTime());

        calendar.setTimeInMillis(lastDate_ms);
        String last_dayNum = dayNumFormat.format(calendar.getTime());

        return now_dayNum.equals(last_dayNum);
    }

}
